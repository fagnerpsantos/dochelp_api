<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['auth:api']], function() {
    Route::resource('pacientes', 'PacienteController');
    Route::get('/pacientes/{id}/avaliacoesPosturais', 'PacienteController@avaliacoesPosturais')->name('pacientes.avaliacoesPosturais');
    Route::get('/pacientes/{id}/avaliacoesFisioterapeuticas', 'PacienteController@avaliacoesFisioterapeuticas')->name('pacientes.avaliacoesFisioterapeuticas');

    Route::resource('avaliacoesFisioterapeuticas', 'AvaliacaoFisioterapeuticaController');
    Route::resource('avaliacoesPosturais', 'AvaliacaoPosturalController');
    Route::resource('funcionarios', 'FuncionarioController');
    Route::resource('empresas', 'EmpresaController');
});
Route::post('/empresas', 'EmpresaController@store');

Route::get('/user', function(){
	$user = Auth::guard('api')->user();
	return $user;
})->middleware('auth:api');


