<?php

use Illuminate\Database\Seeder;

class PacienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pacientes')->insert([
            "nome" => "Paciente de testes",
             "rg" => "123456",
             "cpf" => "123456789",
             "celular" => "1122",
            "dataNascimento" => "1990-01-01",
             "ocupacao" => "Programador",
             "estadoCivil" => "Solteiro",
             "email" => "paciente1@mail.com",
             "medicoResponsavel" => "Seu zé",
            "atividadeFisica" => "Não",
             "medicacao" => "Não",
             "peso" => 80,
             "altura" => 1.70,
             "rua" => "Rua F",
             "bairro" => "Felícia",
             "cidade" => "Conquista",
             "estado" => "Bahia",
             "empresa_id" => 1
        ]);
    }
}
