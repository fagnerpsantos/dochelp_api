<?php

use Illuminate\Database\Seeder;

class FuncionarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('funcionarios')->insert([
            "nome" => "Fagner",
            "email" => "fagner@mail.com",
            "password" => 
            Hash::make("123456"),
            "cpf" => "111222",
            "rg" => "12",
            "celular" => "11222",
            "cargo" => "Programador",
            "estadoCivil" => "Solteiro",
            "dataNascimento" => "1994-08-06",
            "sexo" => "masculino",
            "empresa_id" => "1"
        ]);
    }
}
