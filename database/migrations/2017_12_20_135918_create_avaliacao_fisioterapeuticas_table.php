<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvaliacaoFisioterapeuticasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacao_fisioterapeuticas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('queixaPrimaria');
            $table->string('queixaSecundaria');
            $table->string('historicoDoencaAtual');
            $table->string('patologiasAssociadas');
            $table->string('tipoDor');
            $table->string('fatoresDor');
            $table->string('limitaDor');
            $table->string('locaisDor');
            $table->string('nivelDor');
            $table->string('flexao');
            $table->string('extensao');
            $table->string('lateroflexaoEsquerda');
            $table->string('lateroflexaoDireita');
            $table->string('rotacaoEsquerda');
            $table->string('rotacaoDireita');
            $table->string('planoEscapular');
            $table->string('auscultaGeral');
            $table->string('auscultaLocal');
            $table->string('musculosTensao');
            $table->string('articulacoesDisfuncao');
            $table->string('palpacaoEstatica');
            $table->string('testesOrtopedicos');
            $table->string('grauForcaReflexo');
            $table->string('examesComplementares');
            $table->integer('paciente_id')->unsigned();
            $table->foreign('paciente_id')->references('id')->on('pacientes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaliacao_fisioterapeuticas');
    }
}
