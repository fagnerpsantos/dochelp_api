<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvaliacaoPosturalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('avaliacao_posturals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('motivoConsulta');
            $table->string('sintomalogiaSecundaria');
            $table->string('historicoDoencaAtual');
            $table->string('antecedentesCirurgicos');
            $table->string('destroCanhoto');
            $table->string('tratamentosAnteriores');
            $table->string('tratamentosAtuais');
            $table->string('tratamentoDentario');
            $table->string('temDores');
            $table->string('doresFrequentes');
            $table->integer('escalaVisualAntologica');
            $table->string('planoSagital');
            $table->string('flechaOccipital');
            $table->string('fechaCervical');
            $table->string('flechaDorsal');
            $table->string('flechaLombar');
            $table->string('planoFrontalOmbro');
            $table->string('planoFrontalPelve');
            $table->string('planoHorizontalOmbro');
            $table->string('planoHorizontalPelve');
            $table->string('limitacaoRotacaoCabeca');
            $table->string('limitacaoForcaExtensores');
            $table->string('posturodinamicaCervicalEsquerdo');
            $table->string('posturodinamicaCervicalDireito');
            $table->string('posturodinamicaToracicaEsquerdo');
            $table->string('posturodinamicaToracicaDireito');
            $table->string('posturodinamicaLombarEsquerdo');
            $table->string('posturodinamicaLombarDireito');
            $table->string('posturodinamicaPelveEsquerdo');
            $table->string('posturodinamicaPelveDireito');
            $table->string('captorPodalInsuficienciaConvergencia');
            $table->string('captorPodalConvergenciaReflexa');
            $table->string('captorPodalOlhoDominante');
            $table->string('captorPodalCoverText');
            $table->string('testeRomberg');
            $table->string('possuiPernaCurta');
            $table->string('fossetasMichaelis');
            $table->string('vistaTangencial');
            $table->string('iliacoPosterior');
            $table->string('parametroMenor');
            $table->string('presencaPernaCurta');
            $table->string('tamanhoPernaCurta');
            $table->string('corrigirPernaCurta');
            $table->string('derrotacaoQuadrilDireito');
            $table->string('derrotacaoQuadrilEsquerdo');
            $table->string('aberturaBucal');
            $table->string('amplitudeBucal');
            $table->string('ruidosArticulares');
            $table->string('alteracaoCranioFacialOlhoMaisBaixo');
            $table->string('alteracaoCranioFacialFissuraLabialMaisBaixa');
            $table->string('alteracaoCranioFacialLinhaVerticalMenor');
            $table->string('alteracaoCranioFacialMordidaCruzada');
            $table->string('alteracaoCranioFacialAusenciaDeDentes');
            $table->string('alteracaoCranioFacialTemporaisMaisForte');
            $table->string('alteracaoCranioFacialFocosDentariosReatogenicos');
            $table->string('heinekenTeste');
            $table->string('microgalvanismos');
            $table->string('cicatrizesExternas');
            $table->string('cicatrizesExternasPulso');
            $table->string('cicatrizesInternasGeral');
            $table->string('cicatrizesInternasLocal');
            $table->integer('paciente_id')->unsigned();
            $table->foreign('paciente_id')->references('id')->on('pacientes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('avaliacao_posturals');
    }
}
