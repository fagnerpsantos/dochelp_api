<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvaliacaoPostural extends Model
{
    protected $fillable = array('motivoConsulta', 'sintomalogiaSecundaria',
        'historicoDoencaAtual', 'antecedentesCirurgicos', 'destroCanhoto', 'tratamentosAnteriores',
        'tratamentosAtuais','tratamentoDentario', 'temDores', 'doresFrequentes', 'escalaVisualAntologica', 'planoSagital',
        'flechaOccipital', 'fechaCervical','flechaDorsal', 'flechaLombar', 'planoFrontalOmbro',
        'planoFrontalPelve', 'planoHorizontalOmbro', 'planoHorizontalPelve', 'limitacaoRotacaoCabeca',
        'limitacaoForcaExtensores','posturodinamicaCervicalEsquerdo', 'posturodinamicaCervicalDireito',
        'posturodinamicaToracicaEsquerdo', 'posturodinamicaToracicaDireito', 'posturodinamicaLombarEsquerdo',
        'posturodinamicaLombarDireito','posturodinamicaPelveEsquerdo', 'posturodinamicaPelveDireito',
        'captorPodalInsuficienciaConvergencia', 'captorPodalConvergenciaReflexa', 'captorPodalOlhoDominante',
        'captorPodalCoverText','testeRomberg', 'possuiPernaCurta', 'fossetasMichaelis', 'vistaTangencial',
        'iliacoPosterior','parametroMenor','presencaPernaCurta', 'tamanhoPernaCurta', 'corrigirPernaCurta',
        'derrotacaoQuadrilDireito', 'derrotacaoQuadrilEsquerdo', 'aberturaBucal', 'amplitudeBucal',
        'ruidosArticulares', 'alteracaoCranioFacialOlhoMaisBaixo','alteracaoCranioFacialFissuraLabialMaisBaixa',
        'alteracaoCranioFacialLinhaVerticalMenor', 'alteracaoCranioFacialMordidaCruzada',
        'alteracaoCranioFacialAusenciaDeDentes','alteracaoCranioFacialTemporaisMaisForte',
        'alteracaoCranioFacialFocosDentariosReatogenicos', 'heinekenTeste', 'microgalvanismos',
        'cicatrizesExternas', 'cicatrizesExternasPulso', 'cicatrizesInternasGeral', 'cicatrizesInternasLocal', 'paciente_id');

    public function paciente(){
        return $this->belongsTo('App\Paciente', 'paciente_id');
    }
}
