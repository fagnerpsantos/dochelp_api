<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;
use HipsterJazzbo\Landlord\BelongsToTenants;

class Funcionario extends Authenticatable
{
    use HasApiTokens, Notifiable, BelongsToTenants;

    protected $fillable = ['nome', 'email', 'password', 'cpf', 'rg', 'celular', 'cargo', 'estadoCivil',
    'dataNascimento', 'sexo', 'empresa_id'];

    protected $hidden = ['password', 'remember_token'];

    public function empresa(){
        return $this->belongsTo(Empresa::class);
    }
}
