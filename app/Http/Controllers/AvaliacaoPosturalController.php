<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Paciente;
use App\AvaliacaoPostural;
use Validator;

class AvaliacaoPosturalController extends Controller
{

    protected function validarAvaliacaoPostural($request){
        $validator = Validator::make($request->all(),[
            "motivoConsulta" => 'required',
            "sintomalogiaSecundaria" => 'required',
            "historicoDoencaAtual" => 'required',
            "antecedentesCirurgicos" => 'required',
            "destroCanhoto" => 'required',
            "tratamentosAnteriores" => 'required',
            "tratamentosAtuais" => 'required',
            "tratamentoDentario" => 'required',
            "temDores" => 'required',
            "doresFrequentes" => 'required',
            "escalaVisualAntologica" => 'required',
            "planoSagital" => 'required',
            "flechaOccipital" => 'required',
            "fechaCervical" => 'required',
            "flechaDorsal" => 'required',
            "flechaLombar" => 'required',
            "planoFrontalOmbro" => 'required',
            "planoFrontalPelve" => 'required',
            "planoHorizontalOmbro" => 'required',
            "planoHorizontalPelve" => 'required',
            "limitacaoRotacaoCabeca" => 'required',
            "limitacaoForcaExtensores" => 'required',
            "posturodinamicaCervicalEsquerdo" => 'required',
            "posturodinamicaCervicalDireito" => 'required',
            "posturodinamicaToracicaEsquerdo" => 'required',
            "posturodinamicaToracicaDireito" => 'required',
            "posturodinamicaLombarEsquerdo" => 'required',
            "posturodinamicaLombarDireito" => 'required',
            "posturodinamicaPelveEsquerdo" => 'required',
            "posturodinamicaPelveDireito" => 'required',
            "captorPodalInsuficienciaConvergencia" => 'required',
            "captorPodalConvergenciaReflexa" => 'required',
            "captorPodalOlhoDominante" => 'required',
            "captorPodalCoverText" => 'required',
            "testeRomberg" => 'required',
            "possuiPernaCurta" => 'required',
            "fossetasMichaelis" => 'required',
            "vistaTangencial" => 'required',
            "iliacoPosterior" => 'required',
            "parametroMenor" => 'required',
            "presencaPernaCurta" => 'required',
            "tamanhoPernaCurta" => 'required',
            "corrigirPernaCurta" => 'required',
            "derrotacaoQuadrilDireito" => 'required',
            "derrotacaoQuadrilEsquerdo" => 'required',
            "aberturaBucal" => 'required',
            "amplitudeBucal" => 'required',
            "ruidosArticulares" => 'required',
            "alteracaoCranioFacialOlhoMaisBaixo" => 'required',
            "alteracaoCranioFacialFissuraLabialMaisBaixa" => 'required',
            "alteracaoCranioFacialLinhaVerticalMenor" => 'required',
            "alteracaoCranioFacialMordidaCruzada" => 'required',
            "alteracaoCranioFacialAusenciaDeDentes" => 'required',
            "alteracaoCranioFacialTemporaisMaisForte" => 'required',
            "alteracaoCranioFacialFocosDentariosReatogenicos" => 'required',
            "heinekenTeste" => 'required',
            "microgalvanismos" => 'required',
            "cicatrizesExternas" => 'required',
            "cicatrizesExternasPulso" => 'required',
            "cicatrizesInternasGeral" => 'required',
            "cicatrizesInternasLocal" => 'required',
            "paciente_id" => 'required',
            ]);
        return $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $qtd = $request['qtd'];
            $page = $request['page'];

            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });

            $avaliacaoPostural = AvaliacaoPostural::with('paciente')->paginate($qtd);
            
            $avaliacaoPostural = $avaliacaoPostural->appends(Request::capture()->except('page')); 

            return response()->json(['avaliacaoPostural'=>$avaliacaoPostural], 200);
        } catch (\Exception $e){
            return response()->json('Ocorreu um erro no servidor', 500);
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
            $validator = $this->validarAvaliacaoPostural($request);
            if($validator->fails()){
                return response()->json(['message'=>'Erro', 
                    'errors' => $validator->errors()], 
                    400);
            }
            $data = $request->only(['motivoConsulta', 'sintomalogiaSecundaria',
            'historicoDoencaAtual', 'antecedentesCirurgicos', 'destroCanhoto', 'tratamentosAnteriores',
            'tratamentosAtuais','tratamentoDentario', 'temDores', 'doresFrequentes', 'escalaVisualAntologica', 'planoSagital',
            'flechaOccipital', 'fechaCervical','flechaDorsal', 'flechaLombar', 'planoFrontalOmbro',
            'planoFrontalPelve', 'planoHorizontalOmbro', 'planoHorizontalPelve', 'limitacaoRotacaoCabeca',
            'limitacaoForcaExtensores','posturodinamicaCervicalEsquerdo', 'posturodinamicaCervicalDireito',
            'posturodinamicaToracicaEsquerdo', 'posturodinamicaToracicaDireito', 'posturodinamicaLombarEsquerdo',
            'posturodinamicaLombarDireito','posturodinamicaPelveEsquerdo', 'posturodinamicaPelveDireito',
            'captorPodalInsuficienciaConvergencia', 'captorPodalConvergenciaReflexa', 'captorPodalOlhoDominante',
            'captorPodalCoverText','testeRomberg', 'possuiPernaCurta', 'fossetasMichaelis', 'vistaTangencial',
            'iliacoPosterior','parametroMenor','presencaPernaCurta', 'tamanhoPernaCurta', 'corrigirPernaCurta',
            'derrotacaoQuadrilDireito', 'derrotacaoQuadrilEsquerdo', 'aberturaBucal', 'amplitudeBucal',
            'ruidosArticulares', 'alteracaoCranioFacialOlhoMaisBaixo','alteracaoCranioFacialFissuraLabialMaisBaixa',
            'alteracaoCranioFacialLinhaVerticalMenor', 'alteracaoCranioFacialMordidaCruzada',
            'alteracaoCranioFacialAusenciaDeDentes','alteracaoCranioFacialTemporaisMaisForte',
            'alteracaoCranioFacialFocosDentariosReatogenicos', 'heinekenTeste', 'microgalvanismos',
            'cicatrizesExternas', 'cicatrizesExternasPulso', 'cicatrizesInternasGeral', 'cicatrizesInternasLocal', 'paciente_id']);
            $avaliacaoPostural = AvaliacaoPostural::create($data);
            if($avaliacaoPostural){
                return response()->json(['data'=> $avaliacaoPostural], 201);
            }else{
                return response()->json(['message'=>'Erro ao criar a avaliacação'], 400);
            }                
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            if($id <= 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $avaliacaoPostural = AvaliacaoPostural::find($id);
            if($avaliacaoPostural){
                return response()->json([$avaliacaoPostural], 200);
            }else{
                return response()->json(['message'=>'A avaliacao com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $validator = $this->validarAvaliacaoPostural($request);
            if($validator->fails()){
                return response()->json(['message'=>'Erro', 
                    'errors' => $validator->errors()], 
                    400);
            }
            $data = $request->only(['motivoConsulta', 'sintomalogiaSecundaria',
            'historicoDoencaAtual', 'antecedentesCirurgicos', 'destroCanhoto', 'tratamentosAnteriores',
            'tratamentosAtuais','tratamentoDentario', 'temDores', 'doresFrequentes', 'escalaVisualAntologica', 'planoSagital',
            'flechaOccipital', 'fechaCervical','flechaDorsal', 'flechaLombar', 'planoFrontalOmbro',
            'planoFrontalPelve', 'planoHorizontalOmbro', 'planoHorizontalPelve', 'limitacaoRotacaoCabeca',
            'limitacaoForcaExtensores','posturodinamicaCervicalEsquerdo', 'posturodinamicaCervicalDireito',
            'posturodinamicaToracicaEsquerdo', 'posturodinamicaToracicaDireito', 'posturodinamicaLombarEsquerdo',
            'posturodinamicaLombarDireito','posturodinamicaPelveEsquerdo', 'posturodinamicaPelveDireito',
            'captorPodalInsuficienciaConvergencia', 'captorPodalConvergenciaReflexa', 'captorPodalOlhoDominante',
            'captorPodalCoverText','testeRomberg', 'possuiPernaCurta', 'fossetasMichaelis', 'vistaTangencial',
            'iliacoPosterior','parametroMenor','presencaPernaCurta', 'tamanhoPernaCurta', 'corrigirPernaCurta',
            'derrotacaoQuadrilDireito', 'derrotacaoQuadrilEsquerdo', 'aberturaBucal', 'amplitudeBucal',
            'ruidosArticulares', 'alteracaoCranioFacialOlhoMaisBaixo','alteracaoCranioFacialFissuraLabialMaisBaixa',
            'alteracaoCranioFacialLinhaVerticalMenor', 'alteracaoCranioFacialMordidaCruzada',
            'alteracaoCranioFacialAusenciaDeDentes','alteracaoCranioFacialTemporaisMaisForte',
            'alteracaoCranioFacialFocosDentariosReatogenicos', 'heinekenTeste', 'microgalvanismos',
            'cicatrizesExternas', 'cicatrizesExternasPulso', 'cicatrizesInternasGeral', 'cicatrizesInternasLocal', 'paciente_id']);
            $avaliacaoPostural = AvaliacaoPostural::find($id);
            if($avaliacaoPostural){
                $avaliacaoPostural->update($data);
                return response()->json(['data'=> $avaliacaoPostural], 200);
            }else{
                return response()->json(['message'=>'A avaliação com id '.$id.' não existe'], 400);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            if($id <= 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $avaliacaoPostural = AvaliacaoPostural::find($id);
            if($avaliacaoPostural){
                $avaliacaoPostural->delete();
                return response()->json([], 204);
            }else{
                return response()->json(['message'=>'A avaliação com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }
}
