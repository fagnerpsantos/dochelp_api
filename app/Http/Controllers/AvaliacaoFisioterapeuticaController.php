<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Paciente;
use App\AvaliacaoFisioterapeutica;
use Validator;
class AvaliacaoFisioterapeuticaController extends Controller
{

    protected function validarAvaliacaoFisioterapeutica($request){
        $validator = Validator::make($request->all(),[
            "queixaPrimaria" => 'required',
            "queixaSecundaria" => 'required',
            "historicoDoencaAtual" => 'required',
            "patologiasAssociadas" => 'required',
            "tipoDor" => 'required',
            "fatoresDor" => 'required',
            "limitaDor" => 'required',
            "locaisDor" => 'required',
            "nivelDor" => 'required',
            "flexao" => 'required',
            "extensao" => 'required',
            "lateroflexaoEsquerda" => 'required',
            "lateroflexaoDireita" => 'required',
            "rotacaoEsquerda" => 'required',
            "rotacaoDireita" => 'required',
            "planoEscapular" => 'required',
            "auscultaGeral" => 'required',
            "auscultaLocal" => 'required',
            "musculosTensao" => 'required',
            "articulacoesDisfuncao" => 'required',
            "palpacaoEstatica" => 'required',
            "testesOrtopedicos" => 'required',
            "grauForcaReflexo" => 'required',
            "examesComplementares" => 'required',
            "paciente_id" => 'required'
            ]);
        return $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $qtd = $request['qtd'];
            $page = $request['page'];

            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });

            $avaliacaoFisioterapeutica = AvaliacaoFisioterapeutica::with('paciente')->paginate($qtd);
            
            $avaliacaoFisioterapeutica = $avaliacaoFisioterapeutica->appends(Request::capture()->except('page')); 

            return response()->json(['avaliacaoFisioterapeutica'=>$avaliacaoFisioterapeutica], 200);
        } catch (\Exception $e){
            return response()->json('Ocorreu um erro no servidor', 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
            $validator = $this->validarAvaliacaoFisioterapeutica($request);
            if($validator->fails()){
                return response()->json(['message'=>'Erro', 
                    'errors' => $validator->errors()], 
                    400);
            }
            $data = $request->only(['queixaPrimaria', 'queixaSecundaria', 'historicoDoencaAtual',
            'patologiasAssociadas', 'tipoDor', 'fatoresDor', 'limitaDor', 'locaisDor', 'nivelDor', 'flexao', 'extensao',
            'lateroflexaoEsquerda','lateroflexaoDireita', 'rotacaoEsquerda', 'rotacaoDireita', 'planoEscapular',
            'auscultaGeral', 'auscultaLocal', 'musculosTensao','articulacoesDisfuncao', 'palpacaoEstatica',
            'testesOrtopedicos', 'grauForcaReflexo', 'examesComplementares', 'paciente_id']);
            $avaliacaoFisioterapeutica = AvaliacaoFisioterapeutica::create($data);
            if($avaliacaoFisioterapeutica){
                return response()->json(['data'=> $avaliacaoFisioterapeutica], 201);
            }else{
                return response()->json(['message'=>'Erro ao criar a avaliacação'], 400);
            }                
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            if($id <= 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $avaliacaoFisioterapeutica = AvaliacaoFisioterapeutica::find($id);
            if($avaliacaoFisioterapeutica){
                return response()->json([$avaliacaoFisioterapeutica], 200);
            }else{
                return response()->json(['message'=>'A avaliacao com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $validator = $this->validarAvaliacaoFisioterapeutica($request);
            if($validator->fails()){
                return response()->json(['message'=>'Erro', 
                    'errors' => $validator->errors()], 
                    400);
            }
            $data = $request->only(['queixaPrimaria', 'queixaSecundaria', 'historicoDoencaAtual',
            'patologiasAssociadas', 'tipoDor', 'fatoresDor', 'limitaDor', 'locaisDor', 'nivelDor', 'flexao', 'extensao',
            'lateroflexaoEsquerda','lateroflexaoDireita', 'rotacaoEsquerda', 'rotacaoDireita', 'planoEscapular',
            'auscultaGeral', 'auscultaLocal', 'musculosTensao','articulacoesDisfuncao', 'palpacaoEstatica',
            'testesOrtopedicos', 'grauForcaReflexo', 'examesComplementares', 'paciente_id']);
            $avaliacaoFisioterapeutica = AvaliacaoFisioterapeutica::find($id);
            if($avaliacaoFisioterapeutica){
                $avaliacaoFisioterapeutica->update($data);
                return response()->json(['data'=> $avaliacaoFisioterapeutica], 200);
            }else{
                return response()->json(['message'=>'A avaliação com id '.$id.' não existe'], 400);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            if($id <= 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $avaliacaoFisioterapeutica = AvaliacaoFisioterapeutica::find($id);
            if($avaliacaoFisioterapeutica){
                $avaliacaoFisioterapeutica->delete();
                return response()->json([], 204);
            }else{
                return response()->json(['message'=>'A avaliação com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }
}
