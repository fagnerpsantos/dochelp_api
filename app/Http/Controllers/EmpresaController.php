<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Empresa;
use App\Funcionario;
use Validator;

class EmpresaController extends Controller
{
    protected function validarEmpresa($request){
        $validator = Validator::make($request->all(),[
            "nome" => 'required',
            "email" => 'required',
            "cnpj" => 'required',
            ]);
        return $validator;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
            $qtd = $request['qtd'];
            $page = $request['page'];

            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });

            $empresa = Empresa::paginate($qtd);
            
            $empresa = $empresa->appends(Request::capture()->except('page')); 

            return response()->json(['empresas'=>$funcionario], 200);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
            $validator = $this->validarEmpresa($request);
            if($validator->fails()){
                return response()->json(['message'=>'Erro', 
                    'errors' => $validator->errors()], 
                    400);
            }
            $data = $request->only(['nome', 'email', 'cnpj']);
            $empresa = Empresa::create($data);
            if($empresa){
                $funcionario = Funcionario::create([
                    'nome' => $empresa->nome,
                    'email' => $empresa->email,
                    'password' => bcrypt("123456789"),
                    'cpf' => rand(1,80000),
                    'rg' => "123456789",
                    'celular' => "123456789",
                    'cargo' => "Administrador",
                    'estadoCivil' => "Não informado",
                    'dataNascimento' => "2018-01-01",
                    'sexo' => "Não informado",
                    'empresa_id' => $empresa->id,
                ]);
                dd($funcionario);
                return response()->json(['data'=> $empresa], 201);
            }else{
                return response()->json(['message'=>'Erro ao criar a empresa'], 400);
            }                
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            if($id <= 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $empresa = Empresa::find($id);
            if($empresa){
                return response()->json([$empresa], 200);
            }else{
                return response()->json(['message'=>'A empresa com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $validator = $this->validarEmpresa($request);
            if($validator->fails()){
                return response()->json(['message'=>'Erro', 
                    'errors' => $validator->errors()], 
                    400);
            }
            $data = $request->only(['nome', 'email', 'cnpj']);
            $empresa = Empresa::find($id);
            if($empresa){
                $empresa->update($data);
                return response()->json(['data'=> $empresa], 200);
            }else{
                return response()->json(['message'=>'A empresa com id '.$id.' não existe'], 400);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            if($id <= 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $empresa = Empresa::find($id);
            if($empresa){
                $empresa->delete();
                return response()->json([], 204);
            }else{
                return response()->json(['message'=>'A empresa com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }
}
