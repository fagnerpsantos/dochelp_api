<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Funcionario;
use Validator;

class FuncionarioController extends Controller
{


    protected function validarFuncionario($request){
        $validator = Validator::make($request->all(),[
            "nome" => 'required',
            "email" => 'required',
            "senha" => 'required',
            "cpf" => 'required',
            "rg" => 'required',
            "celular" => 'required',
            "cargo" => 'required',
            "estadoCivil" => 'required',
            "dataNascimento" => 'required',
            "sexo" => 'required',
            "empresa_id" => 'required'
            ]);
        return $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
            $qtd = $request['qtd'];
            $page = $request['page'];

            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });

            $funcionario = Funcionario::paginate($qtd);
            
            $funcionario = $funcionario->appends(Request::capture()->except('page')); 

            return response()->json(['funcionarios'=>$funcionario], 200);
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
            $validator = $this->validarFuncionario($request);
            if($validator->fails()){
                return response()->json(['message'=>'Erro', 
                    'errors' => $validator->errors()], 
                    400);
            }
            $data = $request->only(['nome', 'email', 'senha', 'cpf', 'rg', 'celular', 'cargo', 'estadoCivil',
            'dataNascimento', 'sexo', 'empresa_id']);
            $nome = $data['nome'];
            $email = $data['email'];
            $senha = bcrypt($data['senha']);
            $cpf = $data['cpf'];
            $rg = $data['rg'];
            $celular = $data['celular'];
            $cargo = $data['cargo'];
            $estadoCivil = $data['estadoCivil'];
            $dataNascimento = $data['dataNascimento'];
            $sexo = $data['sexo'];
            $empresa_id = $data['empresa_id'];
            $funcionario = Funcionario::create([
                'nome' => $nome,
                'email' => $email,
                'password' => $senha,
                'cpf' => $cpf,
                'rg' => $rg,
                'celular' => $celular,
                'cargo' => $cargo,
                'estadoCivil' => $estadoCivil,
                'dataNascimento' => $dataNascimento,
                'sexo' => $sexo,
                'empresa_id' => $empresa_id
            ]);
            if($funcionario){
                return response()->json(['data'=> $funcionario], 201);
            }else{
                return response()->json(['message'=>'Erro ao criar a avaliacação'], 400);
            }                
         
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            if($id <= 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $funcionario = Funcionario::find($id);
            if($funcionario){
                return response()->json([$funcionario], 200);
            }else{
                return response()->json(['message'=>'O funcionario com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $validator = $this->validarFuncionario($request);
            if($validator->fails()){
                return response()->json(['message'=>'Erro', 
                    'errors' => $validator->errors()], 
                    400);
            }
            $data = $request->only(['nome', 'email', 'senha', 'cpf', 'rg', 'celular', 'cargo', 'estadoCivil',
            'dataNascimento', 'sexo', 'empresa_id']);
            $funcionario = Funcionario::find($id);
            if($funcionario){
                $funcionario->update($data);
                return response()->json(['data'=> $funcionario], 200);
            }else{
                return response()->json(['message'=>'O funcionario com id '.$id.' não existe'], 400);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            if($id <= 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $funcionario = Funcionario::find($id);
            if($funcionario){
                $funcionario->delete();
                return response()->json([], 204);
            }else{
                return response()->json(['message'=>'O funcionario com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }
}
