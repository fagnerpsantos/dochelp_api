<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use App\Paciente;
use Validator;

class PacienteController extends Controller
{
    protected function validarPaciente($request){
        $validator = Validator::make($request->all(),[
            "nome" => 'required',
            "rg" => 'required',
            "cpf" => 'required',
            "celular" => 'required',
            "dataNascimento" => 'required',
            "ocupacao" => 'required',
            "estadoCivil" => 'required',
            "email" => 'required',
            "medicoResponsavel" => 'required',
            "atividadeFisica" => 'required',
            "medicacao" => 'required',
            "peso" => 'required',
            "altura" => 'required',
            "rua" => 'required',
            "bairro" => 'required',
            "cidade" => 'required',
            "estado" => 'required'
            ]);
        return $validator;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $qtd = $request['qtd'];
            $page = $request['page'];

            Paginator::currentPageResolver(function () use ($page) {
                return $page;
            });

            $pacientes = Paciente::paginate($qtd);
            
            $pacientes = $pacientes->appends(Request::capture()->except('page')); 

            return response()->json(['pacientes'=>$pacientes], 200);
        } catch (\Exception $e){
            return response()->json('Ocorreu um erro no servidor', 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validator = $this->validarPaciente($request);
            if($validator->fails()){
                return response()->json(['message'=>'Erro', 
                    'errors' => $validator->errors()], 
                    400);
            }
            $data = $request->only(['nome', 'rg', 'cpf', 'celular',
            'dataNascimento', 'ocupacao', 'estadoCivil', 'email', 'medicoResponsavel',
            'atividadeFisica', 'medicacao', 'peso', 'altura', 'rua', 'bairro', 'cidade', 'estado']);
            $paciente = Paciente::create($data);
            if($paciente){
                return response()->json(['data'=> $paciente], 201);
            }else{
                return response()->json(['message'=>'Erro ao criar o paciente'], 400);
            }                
        }catch (\Exception $e){
            return response()->json('Ocorreu um erro no servidor', 500);
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            if($id <= 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $paciente = Paciente::find($id);
            if($paciente){
                return response()->json(['data'=>$paciente], 200);
            }else{
                return response()->json(['message'=>'O paciente com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
            }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $validator = $this->validarPaciente($request);
            if($validator->fails()){
                return response()->json(['message'=>'Erro', 
                    'errors' => $validator->errors()], 
                    400);
            }
            $data = $request->only(['nome', 'rg', 'cpf', 'celular',
            'dataNascimento', 'ocupacao', 'estadoCivil', 'email', 'medicoResponsavel',
            'atividadeFisica', 'medicacao', 'peso', 'altura', 'rua', 'bairro', 'cidade', 'estado']);
            $paciente = Paciente::find($id);
            if($paciente){
                $paciente->update($data);
                return response()->json(['data'=> $paciente], 200);
            }else{
                return response()->json(['message'=>'O paciente com id '.$id.' não existe'], 400);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            if($id <= 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $paciente = Paciente::find($id);
            if($paciente){
                $paciente->delete();
                return response()->json([], 204);
            }else{
                return response()->json(['message'=>'O paciente com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
                return response()->json('Ocorreu um erro no servidor', 500);
        }
    }

    public function avaliacoesPosturais($id)
    {
        try{
            if($id < 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $paciente = Paciente::find($id);
            if($paciente){
                return response()->json(['data'=>$paciente->avaliacoesPosturais], 200);
            }else{
                return response()->json(['message'=>'O paciente com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
            return response()->json(['message'=>'Ocorreu um erro no servidor'], 500);
        }
        
    }

    public function avaliacoesFisioterapeuticas($id)
    {
        try{
            if($id < 0){
                return response()->json(['message'=>'ID menor que zero, por favor, informe um ID válido'], 400);
            }
            $paciente = Paciente::find($id);
            if($paciente){
                return response()->json(['data'=>$paciente->avaliacoesFisioterapeuticas], 200);
            }else{
                return response()->json(['message'=>'O paciente com id '.$id.' não existe'], 404);
            }
        }catch (\Exception $e){
            return response()->json(['message'=>'Ocorreu um erro no servidor'], 500);
        }
        
    }
}
