<?php

namespace App\Http\Middleware;

use Closure;

class AddEmpresaTenant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->is('api/*')){
            $user = \Auth::guard('api')->user();
            if($user){                
                $empresa = $user->empresa;
                \Landlord::addTenant($empresa);
                \Landlord::applyTenantScopes($user);
            }
        }
        return $next($request);
    }
}
