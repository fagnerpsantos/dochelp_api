<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvaliacaoFisioterapeutica extends Model
{
    protected $fillable = array('queixaPrimaria', 'queixaSecundaria', 'historicoDoencaAtual',
    'patologiasAssociadas', 'tipoDor', 'fatoresDor', 'limitaDor', 'locaisDor', 'nivelDor', 'flexao', 'extensao',
    'lateroflexaoEsquerda','lateroflexaoDireita', 'rotacaoEsquerda', 'rotacaoDireita', 'planoEscapular',
    'auscultaGeral', 'auscultaLocal', 'musculosTensao','articulacoesDisfuncao', 'palpacaoEstatica',
    'testesOrtopedicos', 'grauForcaReflexo', 'examesComplementares', 'paciente_id');

    public function paciente(){
        return $this->belongsTo('App\Paciente', 'paciente_id');
    }
}
