<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenants;

class Paciente extends Model
{
    use BelongsToTenants;

    protected $fillable = array('nome', 'rg', 'cpf', 'celular',
    'dataNascimento', 'ocupacao', 'estadoCivil', 'email', 'medicoResponsavel',
    'atividadeFisica', 'medicacao', 'peso', 'altura', 'rua', 'bairro', 'cidade', 'estado', 'empresa_id');

    public function avaliacoesFisioterapeuticas(){
        return $this->hasMany('App\AvaliacaoFisioterapeutica', 'paciente_id');
    }
    public function avaliacoesPosturais(){
        return $this->hasMany('App\AvaliacaoPostural', 'paciente_id');
    }
}
