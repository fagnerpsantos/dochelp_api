<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = ['nome', 'email', 'cnpj'];

    public function funcionarios(){
        return $this->hasMany(Funcionarios::class);
    }
}
